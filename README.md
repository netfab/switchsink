
**switchsink**, command line utility to switch process audio sink

Usage
=====

```
 // select the desired window and sink_shortcut using xprop and zenity utilities
 $ bash switchsink
```

```
 // switch <appname> audio to <sink_shortcut|sink_name> sink
 $ bash switchsink <appname> <sink_shortcut|sink_name>
```

```
 // switch <appname> audio to next available sink
 $ bash switchsink <appname> next
```

F.A.Q.
======
 - How do I get \<appname\> ?

```
$ LANG=C /usr/bin/pactl list sink-inputs | grep -e 'application\.name'
```

 - How do I get <sink_name> ?
```
$ LANG=C /usr/bin/pactl list sinks | grep -C 2 Name
```

 - How do I get/define <sink_shortcut> ?

    You must define shortcuts at the beginning of the 'switchsink' file. Run:
```
$ head -n 40 switchsink | less
```

Examples
========
```
$ bash switchsink Firefox next
switchsink: switching Firefox to sink alsa_output.usb-Logitech_G510s_Gaming_Keyboard-02.analog-stereo
switchsink: running: /usr/bin/pactl move-sink-input 195 55
```

```
$ bash switchsink Audacious system
switchsink: switching Audacious to sink alsa_output.pci-0000_00_1b.0.analog-stereo
switchsink: running: /usr/bin/pactl move-sink-input 330 alsa_output.pci-0000_00_1b.0.analog-stereo
```
